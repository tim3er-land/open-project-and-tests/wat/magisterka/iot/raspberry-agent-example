package pl.witowski.wat.mgr.agents.dto;

import jade.core.AID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AgentDataDto {
    private String messageUid;
    private String creationDate;
    private HashMap<String, Object> payload;
    private AID agentNameFrom;
    private String agentNameTo;
}
