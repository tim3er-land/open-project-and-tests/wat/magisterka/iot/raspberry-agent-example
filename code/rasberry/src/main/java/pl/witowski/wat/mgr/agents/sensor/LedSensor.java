package pl.witowski.wat.mgr.agents.sensor;

import com.pi4j.io.gpio.*;

public class LedSensor {
    GpioController gpio;
    private GpioPinDigitalOutput redOutput;
    private GpioPinDigitalOutput blueOutput;
    private GpioPinDigitalOutput greenOutput;

    public void setUp() {
        gpio = GpioFactory.getInstance();
        redOutput = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_29, "MyLED", PinState.LOW);
        greenOutput = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_28, "MyLED", PinState.LOW);
        blueOutput = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_27, "MyLED", PinState.LOW);

    }

    public void setUpLed(Short color) {
        switch (color) {
            case 0:
                redOutput.low();
                blueOutput.low();
                greenOutput.low();
                break;
            case 1:
                redOutput.low();
                blueOutput.low();
                greenOutput.low();
                redOutput.high();
                break;
            case 2:
                redOutput.low();
                blueOutput.low();
                greenOutput.high();
                blueOutput.low();
                break;
            case 3:
                redOutput.low();
                blueOutput.high();
                greenOutput.low();
                greenOutput.low();
                break;
        }
    }
}
