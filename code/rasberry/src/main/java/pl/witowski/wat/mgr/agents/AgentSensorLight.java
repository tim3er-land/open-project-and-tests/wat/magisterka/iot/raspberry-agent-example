package pl.witowski.wat.mgr.agents;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;
import pl.witowski.wat.mgr.agents.core.Behaviour.SendDataBehaviour;
import pl.witowski.wat.mgr.agents.core.OrderEnum;
import pl.witowski.wat.mgr.agents.core.agent.AbstrackAgent;
import pl.witowski.wat.mgr.agents.dto.AgentDataDto;
import pl.witowski.wat.mgr.agents.sensor.LedSensor;

import java.util.HashMap;

public class AgentSensorLight extends AbstrackAgent {
    private Short light;
    private LedSensor sensor;

    @Override
    protected void setup() {
        sensor = new LedSensor();
        super.setup();
        this.addBehaviour(new AgentSensorLightBehavior(this, 1000));
        double randomNumber = this.getRandomNumber(0, 100);
        if (randomNumber <= 25) {
            light = 0; // OFF
        }
        if (randomNumber > 25 && randomNumber <= 50) {
            light = 1; //RED
        }
        if (randomNumber > 50 && randomNumber <= 75) {
            light = 2; //GREEN
        }
        if (randomNumber > 75) {
            light = 3; //BLUE
        }
        this.sensor.setUp();
        this.sensor.setUpLed(light);

    }

    @Override
    public void onMessage(AgentDataDto message, ACLMessage msg) {
        HashMap<String, Object> hashMap = message.getPayload();
        System.out.println(message.getPayload());
        if (hashMap.containsKey(OrderEnum.LIGHT_OFF.name())) {
            this.light = 0;
        }
        if (hashMap.containsKey(OrderEnum.LIGHT_ON_RED.name())) {
            this.light = 1;
        }
        if (hashMap.containsKey(OrderEnum.LIGHT_ON_GREEN.name())) {
            this.light = 2;
        }
        if (hashMap.containsKey(OrderEnum.LIGHT_ON_BLUE.name())) {
            this.light = 3;
        }
        this.sensor.setUpLed(this.light);

    }

    private class AgentSensorLightBehavior extends TickerBehaviour {
        private AgentSensorLight agentSensorLight;

        public AgentSensorLightBehavior(Agent a, long period) {
            super(a, period);
            this.agentSensorLight = (AgentSensorLight) a;
        }

        @Override
        protected void onTick() {
            HashMap<String, Object> payload = new HashMap<>();
            payload.put("light", agentSensorLight.light);
            this.myAgent.addBehaviour(new SendDataBehaviour(this.myAgent, payload));
        }
    }
}
