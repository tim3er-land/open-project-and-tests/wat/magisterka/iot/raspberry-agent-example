package pl.witowski.wat.mgr.agents.sensor;

import com.pi4j.io.w1.W1Master;
import com.pi4j.temperature.TemperatureScale;


public class TemperatureSensor {
    private W1Master w1Master;

    public TemperatureSensor() {
        this.w1Master = new W1Master();
    }

    public double getTemperature() {
        for (com.pi4j.component.temperature.TemperatureSensor device : w1Master.getDevices(com.pi4j.component.temperature.TemperatureSensor.class)) {
            return device.getTemperature(TemperatureScale.CELSIUS);
        }
        return 0;
    }

}
