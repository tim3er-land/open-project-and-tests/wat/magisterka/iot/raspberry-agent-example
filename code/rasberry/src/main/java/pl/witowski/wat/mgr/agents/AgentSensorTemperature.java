package pl.witowski.wat.mgr.agents;

import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import pl.witowski.wat.mgr.agents.core.Behaviour.SendDataBehaviour;
import pl.witowski.wat.mgr.agents.core.agent.AbstrackAgent;
import pl.witowski.wat.mgr.agents.sensor.TemperatureSensor;

import java.util.HashMap;

public class AgentSensorTemperature extends AbstrackAgent {
    private TemperatureSensor sensor;

    @Override
    protected void setup() {
        super.setup();
        this.addBehaviour(new AgentSensorTemperatureBehavior(this, 1000));
        this.sensor = new TemperatureSensor();
    }

    private class AgentSensorTemperatureBehavior extends TickerBehaviour {


        public AgentSensorTemperatureBehavior(Agent a, long period) {
            super(a, period);
        }

        @Override
        protected void onTick() {
            HashMap<String, Object> payload = new HashMap<>();
            payload.put("temperature", sensor.getTemperature());
            this.myAgent.addBehaviour(new SendDataBehaviour(this.myAgent, payload));
        }
    }
}
