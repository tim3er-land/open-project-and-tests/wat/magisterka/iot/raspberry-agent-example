package pl.witowski.wat.mgr.agents.core.Behaviour;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;

import java.util.UUID;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BasicBehavior extends OneShotBehaviour {
    private static final Logger LOGGER = Logger.getLogger(BasicBehavior.class.getName());

    static {
        System.setProperty("java.util.logging.SimpleFormatter.format", "[%1$tF %1$tT %1$tL] [%4$-7s] %5$s %n");
        Logger root = Logger.getLogger(BasicBehavior.class.getName());
        root.setLevel(Level.ALL);
        for (Handler handler : root.getHandlers()) {
            if (handler.getClass().getCanonicalName().contains("pl.wat.mgr"))
                handler.setLevel(Level.ALL);
        }
    }

    protected ObjectMapper objectMapper;

    public BasicBehavior(Agent a) {
        super(a);
        this.objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
    }

    @Override
    public void action() {
        try {
            sendMessage(UUID.randomUUID().toString());
        } catch (Exception ex) {
            LOGGER.log(Level.INFO, "Error occured send data", ex);
            ex.printStackTrace();
        }
    }

    protected void sendMessage(String messageUid) throws Exception {
        LOGGER.info("Not implement sendMessage with uid:" + messageUid);
    }
}
