package pl.witowski.wat.mgr.agents.core;

public enum OrderEnum {
    CHANGE_TEMPERATUTE, LIGHT_OFF, LIGHT_ON_RED, LIGHT_ON_GREEN, LIGHT_ON_BLUE
}
