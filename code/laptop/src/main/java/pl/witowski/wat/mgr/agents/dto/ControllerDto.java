package pl.witowski.wat.mgr.agents.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ControllerDto {
    private String name;
    private String address;
}
