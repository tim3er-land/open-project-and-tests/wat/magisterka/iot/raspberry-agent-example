package pl.witowski.wat.mgr.agents.emulators;

import pl.witowski.wat.mgr.agents.core.Behaviour.SendDataBehaviour;
import pl.witowski.wat.mgr.agents.core.OrderEnum;
import pl.witowski.wat.mgr.agents.core.agent.AbstrackAgent;
import pl.witowski.wat.mgr.agents.dto.AgentDataDto;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;

import java.util.HashMap;

public class AgentEmulatorTemperature extends AbstrackAgent {
    public Double initTemp;

    @Override
    protected void setup() {
        super.setup();
        this.addBehaviour(new AgentEmulatorTemperatureBehavior(this, 1000));
        this.initTemp = getRandomNumber(18, 22);
    }

    @Override
    public void onMessage(AgentDataDto message, ACLMessage msg) {
        HashMap<String, Object> hashMap = message.getPayload();
        if (hashMap.containsKey(OrderEnum.CHANGE_TEMPERATUTE.name())) {
            this.initTemp += (Double) hashMap.get(OrderEnum.CHANGE_TEMPERATUTE.name());
        }
    }

    private class AgentEmulatorTemperatureBehavior extends TickerBehaviour {

        private AgentEmulatorTemperature agentEmulatorTemperature;

        public AgentEmulatorTemperatureBehavior(Agent a, long period) {
            super(a, period);
            this.agentEmulatorTemperature = (AgentEmulatorTemperature) a;
        }

        @Override
        protected void onTick() {
            HashMap<String, Object> payload = new HashMap<>();
            payload.put("temperature", agentEmulatorTemperature.initTemp);
            for (AID aid : this.agentEmulatorTemperature.getAgentsNameList()) {
                this.myAgent.addBehaviour(new SendDataBehaviour(this.myAgent, payload, aid));
            }
            this.agentEmulatorTemperature.initTemp += agentEmulatorTemperature.getRandomNumber(-0.5, 0.5);
        }
    }
}
