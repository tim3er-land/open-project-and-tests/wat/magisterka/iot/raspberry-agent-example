package pl.witowski.wat.mgr.agents.emulators;

import pl.witowski.wat.mgr.agents.core.Behaviour.SendDataBehaviour;
import pl.witowski.wat.mgr.agents.core.OrderEnum;
import pl.witowski.wat.mgr.agents.core.agent.AbstrackAgent;
import pl.witowski.wat.mgr.agents.dto.AgentDataDto;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;

import java.util.HashMap;

public class AgentEmulatorLight extends AbstrackAgent {
    public Short light;

    @Override
    protected void setup() {
        super.setup();
        this.addBehaviour(new AgentEmulatorLightBehavior(this, 1000));
        double randomNumber = this.getRandomNumber(0, 100);
        if (randomNumber <= 25) {
            light = 0; // OFF
        }
        if (randomNumber > 25 && randomNumber <= 50) {
            light = 1; //RED
        }
        if (randomNumber > 50 && randomNumber <= 75) {
            light = 2; //GREEN
        }
        if (randomNumber > 75) {
            light = 3; //blue
        }
    }

    @Override
    public void onMessage(AgentDataDto message, ACLMessage msg) {
        HashMap<String, Object> hashMap = message.getPayload();
        System.out.println(message.getPayload());
        if (hashMap.containsKey(OrderEnum.LIGHT_OFF.name())) {
            this.light = 0;
        }
        if (hashMap.containsKey(OrderEnum.LIGHT_ON_RED.name())) {
            this.light = 1;
        }
        if (hashMap.containsKey(OrderEnum.LIGHT_ON_GREEN.name())) {
            this.light = 2;
        }
        if (hashMap.containsKey(OrderEnum.LIGHT_ON_BLUE.name())) {
            this.light = 3;
        }
    }

    private class AgentEmulatorLightBehavior extends TickerBehaviour {
        private AgentEmulatorLight agentEmulatorLight;

        public AgentEmulatorLightBehavior(Agent a, long period) {
            super(a, period);
            this.agentEmulatorLight = (AgentEmulatorLight) a;
        }

        @Override
        protected void onTick() {
            HashMap<String, Object> payload = new HashMap<>();
            payload.put("light", agentEmulatorLight.light);
            for (AID aid : this.agentEmulatorLight.getAgentsNameList()) {
                this.myAgent.addBehaviour(new SendDataBehaviour(this.myAgent, payload, aid));
            }
        }
    }
}
