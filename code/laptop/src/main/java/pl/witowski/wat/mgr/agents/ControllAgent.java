package pl.witowski.wat.mgr.agents;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import pl.witowski.wat.mgr.agents.core.Behaviour.SendDataBehaviour;
import pl.witowski.wat.mgr.agents.core.OrderEnum;
import pl.witowski.wat.mgr.agents.core.agent.AbstrackAgent;
import pl.witowski.wat.mgr.agents.dto.ControllerDto;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import lombok.SneakyThrows;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class ControllAgent extends AbstrackAgent {
    private ObjectMapper objectMapper;
    Map<String, ControllerDto> agents;

    @Override
    @SneakyThrows
    protected void setup() {
        IOT_AGENT = "IOT-MANAGER";
        super.setup();
        this.agents = new HashMap<>();
        String json = readFileInList("config.json");
        this.addBehaviour(new ControllBehavior(this, 5000, this));
        this.objectMapper = new ObjectMapper();
        this.objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        this.objectMapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
        agents=objectMapper.readValue(json, new TypeReference<Map<String, ControllerDto>>() {
        });
    }

    private class ControllBehavior extends TickerBehaviour {
        ControllAgent controllAgent;

        public ControllBehavior(Agent a, long period, ControllAgent controllAgent) {
            super(a, period);
            this.controllAgent = controllAgent;
        }


        @Override
        protected void onTick() {
            try {
                int waitForXSeconds = 5;
                String prettyJsonString = gson.toJson(this.controllAgent.agents);
                System.out.println(prettyJsonString);
                BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
                long startTime = System.currentTimeMillis();
                while ((System.currentTimeMillis() - startTime) < waitForXSeconds * 1000
                        && !in.ready()) {
                }
                if (in.ready()) {
                    String line = in.readLine();
                    System.out.println("You entered: " + line);
                    if(line==null || line.isEmpty())
                        return;
                    String[] strings = line.split("\\s");
                    if (strings.length == 2) {
                        AID dest = new AID(this.controllAgent.agents.get(strings[0]).getName(), AID.ISGUID);
                        dest.addAddresses(this.controllAgent.agents.get(strings[0]).getAddress());
                        System.out.println(dest.toString());
                        sendControll(strings[1], dest);
                    }
                } else {
                    System.out.println("You did not enter data");
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        private void sendControll(String in, AID agent) {
            if (in.equalsIgnoreCase("TD")) {
                HashMap sendMap = new HashMap();
                sendMap.put(OrderEnum.CHANGE_TEMPERATUTE.name(), -5.0);
                this.controllAgent.addBehaviour(new SendDataBehaviour(this.controllAgent, sendMap, agent));
            } else if (in.equalsIgnoreCase("TU")) {
                HashMap sendMap = new HashMap();
                sendMap.put(OrderEnum.CHANGE_TEMPERATUTE.name(), 5.0);
                this.controllAgent.addBehaviour(new SendDataBehaviour(this.controllAgent, sendMap, agent));
            } else if (in.equalsIgnoreCase("LF")) {
                HashMap sendMap = new HashMap();
                sendMap.put(OrderEnum.LIGHT_OFF.name(), OrderEnum.LIGHT_OFF.name());
                this.controllAgent.addBehaviour(new SendDataBehaviour(this.controllAgent, sendMap, agent));
            } else if (in.equalsIgnoreCase("LNR")) {
                HashMap sendMap = new HashMap();
                sendMap.put(OrderEnum.LIGHT_ON_RED.name(), OrderEnum.LIGHT_ON_RED.name());
                this.controllAgent.addBehaviour(new SendDataBehaviour(this.controllAgent, sendMap, agent));
            } else if (in.equalsIgnoreCase("LNB")) {
                HashMap sendMap = new HashMap();
                sendMap.put(OrderEnum.LIGHT_ON_BLUE.name(), OrderEnum.LIGHT_ON_BLUE.name());
                this.controllAgent.addBehaviour(new SendDataBehaviour(this.controllAgent, sendMap, agent));
            } else if (in.equalsIgnoreCase("LNG")) {
                HashMap sendMap = new HashMap();
                sendMap.put(OrderEnum.LIGHT_ON_GREEN.name(), OrderEnum.LIGHT_ON_GREEN.name());
                this.controllAgent.addBehaviour(new SendDataBehaviour(this.controllAgent, sendMap, agent));
            }

        }
    }
}
