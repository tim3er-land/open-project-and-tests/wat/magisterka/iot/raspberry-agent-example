package pl.witowski.wat.mgr.agents.core.Behaviour;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import pl.witowski.wat.mgr.agents.core.agent.AbstrackAgent;
import pl.witowski.wat.mgr.agents.dto.AgentDataDto;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;

import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author: Piotr Witowski
 * @date: 02.12.2021
 * @project simple-jade-projectv2
 * Day of week: czwartek
 */
public class ReceiveDataBehaviour extends CyclicBehaviour {
    private static final Logger LOGGER = Logger.getLogger(ReceiveDataBehaviour.class.getName());

    static {
        System.setProperty("java.util.logging.SimpleFormatter.format", "[%1$tF %1$tT %1$tL] [%4$-7s] %5$s %n");
        Logger root = Logger.getLogger(ReceiveDataBehaviour.class.getName());
        root.setLevel(Level.ALL);
        for (Handler handler : root.getHandlers()) {
            if (handler.getClass().getCanonicalName().contains("pl.wat.mgr"))
                handler.setLevel(Level.ALL);
        }
    }

    private final ObjectMapper objectMapper;
    private AbstrackAgent agent;

    public ReceiveDataBehaviour(AbstrackAgent a) {
        super(a);
        this.agent = a;
        this.objectMapper = new ObjectMapper();
        this.objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        this.objectMapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
    }

    @Override
    public void action() {
        ACLMessage msg = myAgent.receive();
        try {
            if (msg != null && msg.getContent() != null && !msg.getContent().isEmpty()) {
                String message = msg.getContent();
                AgentDataDto agentDataDto = this.objectMapper.readValue(message, AgentDataDto.class);
                this.agent.onMessage(agentDataDto,msg);
//                LOGGER.info("receive message " + message);
            } else {
                block();
            }
        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, "Error occured processed message", ex);
            block();
        }
    }
}
