package pl.witowski.wat.mgr.agents.core.agent;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import pl.witowski.wat.mgr.agents.core.Behaviour.ReceiveDataBehaviour;
import pl.witowski.wat.mgr.agents.core.Behaviour.SearchAgentBehavior;
import pl.witowski.wat.mgr.agents.dto.AgentDataDto;
import jade.core.AID;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * @author: Piotr Witowski
 * @date: 04.12.2021
 * @project simple-jade-projectv2
 * Day of week: sobota
 */
public abstract class AbstrackAgent extends Agent {

    private static final Logger LOGGER = Logger.getLogger(AbstrackAgent.class.getName());
    public static String IOT_AGENT = "IoT-agent";
    protected Gson gson;
    static {
        System.setProperty("java.util.logging.SimpleFormatter.format", "[%1$tF %1$tT %1$tL] [%4$-7s] %5$s %n");
        Logger root = Logger.getLogger(AbstrackAgent.class.getName());
        root.setLevel(Level.ALL);
        for (Handler handler : root.getHandlers()) {
            if (handler.getClass().getCanonicalName().contains("pl.wat.mgr"))
                handler.setLevel(Level.ALL);
        }
    }


    protected List<AID> agentsNameList;

    public AbstrackAgent() {
        this.agentsNameList = new ArrayList<>();
    }

    @Override
    protected void setup() {
        this.gson = new GsonBuilder().setPrettyPrinting().create();

        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());
        ServiceDescription sd = new ServiceDescription();
        sd.setType(IOT_AGENT);
        sd.setName(getLocalName() + "-" + IOT_AGENT);
        dfd.addServices(sd);
        try {
            DFService.register(this, dfd);
        } catch (FIPAException fe) {
            fe.printStackTrace();
        }
        addBehaviour(new ReceiveDataBehaviour(this));
        addBehaviour(new SearchAgentBehavior(this, 1000));
        LOGGER.info("start setup agent with uid " + this.getLocalName());
        super.setup();
    }

    public void onMessage(AgentDataDto message, ACLMessage msg) {
        LOGGER.log(Level.INFO, message.toString());
    }

    public List<AID> getAgentsNameList() {
        return agentsNameList;
    }

    public void setAgentsNameList(List<AID> agentsNameList) {
        this.agentsNameList = agentsNameList;
    }

    public double getRandomNumber(double min, double max) {
        return ThreadLocalRandom.current().nextDouble(min, max);
    }

    public static String readFileInList(String fileName) {

        List<String> lines = Collections.emptyList();
        try {
            lines = Files.readAllLines(Paths.get(fileName), StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return lines.stream().collect(Collectors.joining(""));
    }
}
