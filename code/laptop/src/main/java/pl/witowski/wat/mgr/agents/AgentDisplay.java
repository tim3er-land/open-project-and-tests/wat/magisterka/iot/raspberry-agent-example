package pl.witowski.wat.mgr.agents;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import pl.witowski.wat.mgr.agents.core.agent.AbstrackAgent;
import pl.witowski.wat.mgr.agents.dto.AgentDataDto;
import jade.core.AID;
import jade.lang.acl.ACLMessage;

import java.util.HashMap;
import java.util.Map;

public class AgentDisplay extends AbstrackAgent {

    private ObjectMapper objectMapper;
    private Map<String, HashMap<String, Object>> payload;

    @Override
    protected void setup() {
        IOT_AGENT = "IOT-DISPLAY";
        super.setup();
        this.objectMapper = new ObjectMapper();
        this.objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        this.objectMapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
        payload = new HashMap<>();
    }


    @Override
    public void onMessage(AgentDataDto message, ACLMessage msg) {
        payload.put(message.getAgentNameFrom().getName(), message.getPayload());
        String prettyJsonString = gson.toJson(payload);
        System.out.println(prettyJsonString);
    }
}
