package pl.witowski.wat.mgr.agents.core.Behaviour;

import pl.witowski.wat.mgr.agents.core.agent.AbstrackAgent;
import jade.core.AID;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;

import java.util.ArrayList;
import java.util.List;

public class SearchAgentBehavior extends TickerBehaviour {
    public SearchAgentBehavior(AbstrackAgent a, long period) {
        super(a, period);
    }

    @Override
    protected void onTick() {
        DFAgentDescription template = new DFAgentDescription();
        ServiceDescription sd = new ServiceDescription();
        sd.setType("IOT-DISPLAY");
        template.addServices(sd);
        try {
            DFAgentDescription[] result = DFService.search(myAgent, template);
            List<AID> aidList = new ArrayList<>();
            for (DFAgentDescription dfAgentDescription : result) {
                AID aid = dfAgentDescription.getName();
                if (!itsNotAgentAID(aid))
                    aidList.add(aid);
            }
            ((AbstrackAgent) this.myAgent).setAgentsNameList(aidList);
        } catch (FIPAException e) {
            throw new RuntimeException(e);
        }
    }

    private boolean itsNotAgentAID(AID aid) {
        if (this.myAgent.getAID().getLocalName().equalsIgnoreCase(aid.getLocalName())) {
            return true;
        }
        return false;
    }
}
