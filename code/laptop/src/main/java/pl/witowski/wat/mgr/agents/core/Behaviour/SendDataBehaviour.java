package pl.witowski.wat.mgr.agents.core.Behaviour;

import com.aventrix.jnanoid.jnanoid.NanoIdUtils;
import pl.witowski.wat.mgr.agents.dto.AgentDataDto;
import jade.core.AID;
import jade.core.Agent;
import jade.lang.acl.ACLMessage;

import java.security.SecureRandom;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author: Piotr Witowski
 * @date: 02.12.2021
 * @project simple-jade-projectv2
 * Day of week: czwartek
 */
public class SendDataBehaviour extends BasicBehavior {
    private static final Logger LOGGER = Logger.getLogger(SendDataBehaviour.class.getName());
    public static char[] ALFANUMERIC_ALPHABET = new char[]{'1', '2', '3', '4', '5', '6', '7', '8', '9', '0'
            , 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};

    static {
        System.setProperty("java.util.logging.SimpleFormatter.format", "[%1$tF %1$tT %1$tL] [%4$-7s] %5$s %n");
        Logger root = Logger.getLogger(SendDataBehaviour.class.getName());
        root.setLevel(Level.ALL);
        for (Handler handler : root.getHandlers()) {
            if (handler.getClass().getCanonicalName().contains("pl.wat.mgr"))
                handler.setLevel(Level.ALL);
        }
    }

    private SecureRandom secureRandom;
    private HashMap<String, Object> payload;
    private AID agentNameTo;

    public SendDataBehaviour(Agent a, HashMap<String, Object> payload, AID agentNameTo) {
        super(a);
        this.payload = payload;
        this.agentNameTo = agentNameTo;
        this.secureRandom = new SecureRandom();
    }

    @Override
    protected void sendMessage(String messageUid) throws Exception {
        AgentDataDto agentRawDataDto = prepareNewAgentDataDto();
        ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
        msg.setContent(objectMapper.writeValueAsString(agentRawDataDto));
        msg.addReceiver(this.agentNameTo);
        LOGGER.info(msg.getContent());
        myAgent.send(msg);
    }



    private AgentDataDto prepareNewAgentDataDto() {
        return AgentDataDto.builder()
                .agentNameFrom(this.myAgent.getAID())
                .messageUid(NanoIdUtils.randomNanoId(secureRandom, ALFANUMERIC_ALPHABET, 6))
                .agentNameTo(this.agentNameTo.getLocalName())
                .payload(this.payload)
                .creationDate(ZonedDateTime.now().format(DateTimeFormatter.ISO_OFFSET_DATE_TIME))
                .build();
    }
}
